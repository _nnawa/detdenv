from eve import Eve
from eve.auth import BasicAuth
from werkzeug.security import generate_password_hash, check_password_hash
from flask import current_app as app


SALT = 'f03d1f0114334f55ada1ff3651ca6b50' # use uuid.uiid4().hex from python console


class AppAuth(BasicAuth):
    def check_auth(self, username, password, allowed_roles, resource, method):
        if resource == 'accounts' and method == 'POST':
            return True

        account = app.data.driver.db['accounts']
        lookup = {'username': username}
        account = account.find_one(lookup)
        return account and check_password_hash(account['password'], password+SALT)


def create_user(documents):
    for document in documents:
        password = document['password']
        document['password'] = generate_password_hash(password+SALT)
