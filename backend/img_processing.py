import numpy as np
import cv2
import base64
import json
import backend.gram_schmidt as gs
from flask import current_app as app



def load_image(filename, resizeable=False, image_size=None):
    img = cv2.imread(filename)
    if resizeable:
        return cv2.resize(img, (image_size['width'], image_size['height']),
                            interpolation=cv2.INTER_CUBIC)
    else:
        return img


def resize_image(img, image_size):
    return cv2.resize(img, image_size, interpolation=cv2.INTER_CUBIC)


def contourToBase64(contours):
    for i, contour in enumerate(contours):
        contours[i] = contour.tolist()
    return base64.b64encode(bytes(json.dumps(contours), 'utf-8'))


def base64toContour(encoded):
    return [np.array(contour) for contour in json.loads(base64.b64decode(encoded))]
    # return np.fromstring(base64.b64decode(encoded), np.uint8)


def base64toImage(encoded):
    nparr = np.fromstring(base64.b64decode(encoded), np.uint8)
    img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    return img


def bufferToImage(buffer):
    nparr = np.fromstring(buffer, np.uint8)
    return cv2.imdecode(nparr, cv2.IMREAD_COLOR)


def imageToBase64(img):
    retval, buffer = cv2.imencode('.jpg', img)
    return base64.b64encode(buffer)


def imageToBuffer(img):
    retval, buffer = cv2.imencode('.jpg', img, [cv2.IMWRITE_JPEG_QUALITY, 50])
    return buffer


def showImage(img, message="Image"):
    cv2.namedWindow(message, cv2.WINDOW_NORMAL)
    cv2.imshow(message, img)
    k = cv2.waitKey(0) & 0xFF
    if k == 27:
        cv2.DestroyAllWindows()


def showHistogram(img):
    pass


def medianBlur(img, kernel):
    return cv2.medianBlur(img, kernel)


def convertToHSV(img):
    return cv2.cvtColor(img, cv2.COLOR_BGR2HSV)


def histogram(img, hist_size, mask=None, img_dim=3, normal_area=1, interval=[[0, 255], [0, 255], [0, 255]]):
    # if  mask and normalize:
    #     cv2.contourArea(detectContours(mask)[0])
    return (cv2.calcHist(\
            [img], [channel], mask, [hist_size], interval[channel]) / normal_area\
            for channel in range(0, img_dim))


def gs_projection(img, roi_vector):
    """
    This projection enhance image contrast towards the region of interest
    @img is image input
    @roi is region of interest, manually added, formatted in array
    """
    # app.logger.info(img)
    # img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    (b, g, r) = cv2.split(img)
    (hb, hg, hr) = histogram(img, 256)

    v1 = np.uint8([hb.argmax(), hg.argmax(), hr.argmax()])
    v2 = np.uint8([b.mean(), g.mean(), r.mean()])
    # v3 = np.array([103, 72, 76]) # blue
    # app.logger.info(gs.gs_projection((v3, v2, v1)))
    # app.logger.info(gs.gs_projection((v2, v3, v1)))
    # app.logger.info(gs.gs_projection((v1, v2, v3)))
    # app.logger.info(gs.gs_projection((v2, v1, v3)))
    # app.logger.info(gs.gs_projection((v1, v3, v2)))
    # app.logger.info(gs.gs_projection((v3, v1, v2)))

    # v1 = np.array([hb.argmax(), hg.argmax(), hr.argmax()])
    # v2 = np.array([b.mean(), g.mean(), r.mean()])
    v3 = np.array(roi_vector)
    # v3 = np.array([101, 63, 66])
    # v3 = np.array([180*15/255, 100*127/255, 100*191/255]) # hsv mode
    # v3 = np.array([27, 73, 120])
    # v1 = np.array([112, 113, 121])
    # v3 = np.array([55, 76, 116]) # red
    #app.logger.info((v1, v2, v3))

    w = gs.gs_projection((v1, v2, v3))
    # w = gs.gs_projection2D((np.array([64, 68, 93]), v3))
    # w = gs.gs_projection2D((v1, v2))
    #app.logger.info(w)
    # app.logger.info(gs.gs_projection((v3, v2, v1)))
    # app.logger.info(gs.gs_projection((v2, v3, v1)))
    # app.logger.info(gs.gs_projection((v1, v2, v3)))
    # app.logger.info(gs.gs_projection((v2, v1, v3)))
    # app.logger.info(gs.gs_projection((v1, v3, v2)))
    # app.logger.info(gs.gs_projection((v3, v1, v2)))


    result = np.inner(w, img)
    # print(result, result.shape)

    return np.uint8(255 * (result - result.min()) / (result.max() -\
                    result.min()))


def adaptiveThreshold(img, kernel_size=333, c=-2):
    """
    optimized thresholding for my undergrad thesis
    """
    return cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_MEAN_C,\
                    cv2.THRESH_BINARY, kernel_size, c)


def otsuThreshold(img, mask=None):
    hist = np.hstack(np.hstack(histogram(img, 256, mask=mask, img_dim=1)))
    hist_norm = hist.ravel() / hist.max()
    Q = hist_norm.cumsum()

    bins = np.arange(256)

    fn_min = np.inf
    value = -1

    for i in range(1, 256):
        p1, p2 = np.hsplit(hist_norm, [i])
        q1, q2 = Q[i], Q[255] - Q[i]
        if q1 > 0 and q2 > 0:
            b1, b2 = np.hsplit(bins, [i])

            # finding mean and variance
            m1, m2 = np.sum(p1 * b1) / q1, np.sum(p2 * b2) / q2
            v1, v2 = np.sum(((b1 - m1) ** 2) * p1) / q1, np.sum(((b2 - m2) ** 2) * p2) / q2

            # calculate minimization function
            fn = v1 * q1 +  v2 * q2
            if fn < fn_min:
                # app.logger.info("{}: q1={}, v1={}, q2={}, v2={}, fn={},".format(i,q1,v1,q2,v2,fn))
                fn_min = fn
                value = i

    temp, thresh = cv2.threshold(img, value, 255, cv2.THRESH_BINARY)
    return value, thresh


def erode(img, kernel_size, iterations=1):
    # kernel = np.ones((kernel_size, kernel_size), np.uint8)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_size, kernel_size))
    return cv2.erode(img, kernel, iterations=iterations)


def dilate(img, kernel_size, iterations=1):
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (kernel_size, kernel_size))
    # kernel = np.ones((kernel_size, kernel_size), np.uint8)
    return cv2.dilate(img, kernel, iterations=iterations)


def detectContours(img):
    """
    detect contours on binary image, return list of contours
    """
    # contours, hrc = cv2.findContours(img, cv2.RETR_LIST,\
    im2, contours, hrc = cv2.findContours(img, cv2.RETR_LIST,\
                                            cv2.CHAIN_APPROX_SIMPLE)
    return contours


def circularity(contour):
    return (4 * np.pi * cv2.contourArea(contour)) /\
            (cv2.arcLength(contour, True) ** 2)


def finalCleaning(contours, img, min_area=6000, circularity_th=0.85):
    cleaned = []
    dirty = []
    for contour in contours:
        # if np.any(contour == 1):
        #     continue
        if cv2.contourArea(contour) < min_area:
            continue
        if  circularity(contour) < circularity_th:
            # app.logger.info((cv2.contourArea(contour), cv2.arcLength(contour, True)))
            # app.logger.info(circularity(contour))
            dirty.append(contour)
            cleaned.extend(extendedCleaning([contour], img, min_area, circularity_th))
            continue
        cleaned.append(contour)

    return cleaned, dirty


def extendedCleaning(contours, img, min_area=6000, circularity_th=0.85):
    cleaned = []
    while len(contours) > 0:
        binary = rasterizeContours([contours.pop()], img)
        masked = andImage(img, img, masking=binary)
        retval, newth = otsuThreshold(masked, mask=binary)
        # showImage(newth)
        newcnts = detectContours(newth)
        # cleaned.extend(newcnts)
        temp, dirty = finalCleaning(newcnts, img, min_area=min_area, circularity_th=circularity_th)
        # contours.extend(dirty)
        cleaned.extend(temp)

    return cleaned


def rasterizeContours(contours, img):
    binary_image = np.zeros(img.shape[:2], np.uint8)
    cv2.drawContours(binary_image, contours, -1, 255, -1)
    return binary_image


def andImage(img_A, img_B, masking=1):
    return cv2.bitwise_and(img_A, img_B, mask=masking)


def cellHistograms(contours, img):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    cell_hists = []
    interval = [[0, 179], [0, 255], [0, 255]]
    for contour in contours:
        contour = np.int32(contour)
        masking = rasterizeContours([contour], img)
        # hsv_hist = histogram(hsv, 16, mask=masking,\
        #                         normal_area=masking.sum()/255)
        # app.logger.info(np.hstack(np.hstack(hsv_hist)).shape)
        #app.logger.info("A: {}".format(masking.sum()//255))

        hsv_hist = [cv2.calcHist([hsv],
                                [channel],
                                masking,
                                [16],
                                interval[channel]) / (masking.sum()/255) \
                                for channel in range(3)]

        cell_hists.append(np.hstack(np.hstack(hsv_hist)))
        #app.logger.info("Histogram: {}".format(hsv_hist))
        # cell_hists.append(np.hstack(hsv_hist))
    return np.array(cell_hists)


def labelMarkImages(contours, img, labels=None):
    font = cv2.FONT_HERSHEY_SIMPLEX
    color = (255, 255, 255)
    positive = (0, 0, 255)
    negative = (255, 0, 0)
    for i, contour in enumerate(contours):
        (x, y), radius = cv2.minEnclosingCircle(contour)
        center = (int(x), int(y))
        color = color if labels is None else positive if labels[i] == 1 else negative
        if labels is not None: cv2.circle(img, center, int(radius), color, 2)

        corner = (int(x - radius / 2), int(y + radius / 2))
        cv2.putText(img, "{}".format(i+1), corner, font, 1, color, 1, cv2.LINE_AA)
    return img


def makeLabel(labels):
    return np.int32(labels) # change to int32 if something is broken


def confusion_matrix(labels, result):
    y = np.array(labels)
    h = np.array(result)

    tp, tn, fp, fn = ((y+h).tolist().count(2), (y+h).tolist().count(-2),
                        (y-h).tolist().count(-2), (y-h).tolist().count(2))
    return tp, tn, fp, fn
