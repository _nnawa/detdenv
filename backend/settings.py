DEBUG=True
# Let's just use the local mongod instance. Edit as needed.

# Please note that MONGO_HOST and MONGO_PORT could very well be left
# out as they already default to a bare bones local 'mongod' instance.
MONGO_HOST = 'localhost'
MONGO_PORT = 27017

# Skip these if your db has no auth. But it really should.
MONGO_USERNAME = ''
MONGO_PASSWORD = ''

MONGO_DBNAME = 'detdenv_db'

URL_PREFIX = 'api'

RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PUT', 'PATCH', 'DELETE']
# PUBLIC_METHODS = ['GET', 'POST']
# PUBLIC_ITEM_METHODS = ['GET']

X_DOMAINS = '*'

EXTENDED_MEDIA_INFO = ['content_type', 'name', 'length']
AUTO_COLLAPSE_MULTI_KEYS = True
AUTO_CREATE_LIST = True
PAGINATION = False
CACHE_CONTROL = 'no-cache, no-store, must-revalidate'
MULTIPART_FORM_FIELDS_AS_JSON = True

accounts_schema = {
    'username': {
        'type': 'string',
        'required': True,
        'unique': True,
    },
    'email': {
        'type': 'string',
        'required': True,
        'unique': True,
    },
    'password': {
        'type': 'string',
        'required': True,
    }
}

accounts = {
    # read-only entry point is accesible at '/accounts/<username>'
    'additional_lookup': {
        'url': 'regex("[\w]+")',
        'field': 'username'
    },

    # disable endpoint caching to prevent apps from caching account data
    # 'cache_control': '',
    'cache_expires': 0,

    'resource_mehods': ['GET', 'POST', 'DELETE'],
    'public_methods': ['GET', 'POST'],

    # schema for the accounts endpoint
    'schema': accounts_schema,
}

images_schema = {
    'filename': {
        'type': 'string',
        'required': True
    },
    'raw_file': {
        'type': 'media',
        'required': True
    },
    'is_processed': {
        'type': 'boolean',
        'default': False
    },
    'is_stepped': {
        'type': 'boolean',
        'default': False
    },
    'is_trained': {
        'type': 'boolean',
        'default': False
    },
    'is_tested': {
        'type': 'boolean',
        'default': False
    },
    'detected': {
        'type': 'media'
    },
    # 'step_processed': {
    #     'type': 'list',
    #     'schema': {
    #         'type': 'media'
    #     }
    # },
    'labels': {
        'type': 'list',
        'schema': {
            'type': 'integer'
        }
    },
    'result': {
        'type': 'list',
        'schema': {
            'type': 'integer'
        }
    },
    'cells': {
        'type': 'string'
    },
    # 'cell_count': {
    #     'type': 'integer'
    # }
}

images = {
    # read-only entry point is accesible at '/accounts/<username>'
    'additional_lookup': {
        'url': 'regex("[\w]+")',
        'field': 'filename'
    },
    # 'cache_control': '',
    'cache_expires': 0,
    'schema': images_schema
}

com = {
    'schema': {
        'command': {
            'type': 'string',
            'allowed': ['process_image', 'post_steps', 'put_steps', 'train_model',
                'test_model', 'post_histogram', 'put_histogram', 'analyze']
        },
        'scope': {
            # 'type': 'string',
            'type': 'list',
            'allowed': ['all', 'single', 'hue', 'saturation', 'value', 'image', 'cell', 'cells', 'red', 'green', 'blue']
        },
        'ref_id': {
            'type': 'string'
        }
    },
    'resource_mehods': ['GET', 'POST', 'DELETE'],
    'item_methods': ['GET', 'DELETE']
}

step_processed = {
    'resource_mehods': ['GET', 'POST', 'DELETE'],
    'item_methods': ['GET', 'PUT', 'DELETE'],
    # 'cache_control': '',
    'cache_expires': 0,
    'schema': {
        'image': {
            'type': 'media',
            'required': True
        },
        'step': {
            'type': 'integer',
            'required': True
        },
        'ref_id': {
            'type': 'objectid',
            'required': True,
            'data_relation': {
                'resource': 'images',
                'field': '_id'
            }
        }
    }
}

histogram = {
    'resource_mehods': ['GET', 'POST', 'DELETE'],
    'item_methods': ['GET', 'PUT', 'DELETE'],
    # 'cache_control': '',
    'cache_expires': 0,
    'schema': {
        'hist': {
            'type': 'list',
            'required': True,
            'schema': {
                'type': 'float'
            }
        },
        # 'image': {
        #     'type': 'media',
        #     'required': True
        # },
        'groups': {
            'type': 'list',
            'required': True,
            'allowed': ['hue', 'saturation', 'value', 'image', 'cell', 'cells', 'red', 'green', 'blue']
        },
        'ref_id': {
            'type': 'objectid',
            'required': True,
            'data_relation': {
                'resource': 'images',
                'field': '_id'
            }
        }
    }
}

processing_config = {
    'hateoas': False,
    'resource_mehods': ['GET', 'POST', 'DELETE'],
    'item_methods': ['GET', 'PATCH'],
    # 'cache_control': '',
    'cache_expires': 0,
    'schema': {
        'a': {
            'type': 'integer',
            'required': True,
        },
        'image_size': {
            'type': 'dict',
            'required': True,
            'default': {
                'height': 1200,
                'width': 1600
            }
            # 'schema': {
            #     'height': {
            #         'type': 'integer',
            #         'default': 1200
            #     },
            #     'width': {
            #         'type': 'integer',
            #         'default': 1600
            #     }
            # }
        },
        'smallest_cell': {
            'type': 'float',
            'required': True,
            'default': 0.002083
        },
        'circularity_th': {
            'type': 'float',
            'required': True,
            'default': 0.75
        },
        'erode_kernel': {
            'type': 'integer',
            'required': True,
            'default': 17
        },
        'dilate_kernel': {
            'type': 'integer',
            'required': True,
            'default': 17
        },
        'median_kernel': {
            'type': 'integer',
            'required': True,
            'default': 77
        },
        'segmentation_method': {
            'type': 'string',
            'allowed': ['adaptive', 'otsu']
        },
        'segmentation_kernel': {
            'type': 'integer',
            'required': True,
            'default': 333
        },
    }
}

svm_model = {
    'resource_mehods': ['GET', 'POST', 'DELETE'],
    'item_methods': ['GET', 'PUT', 'PATCH', 'DELETE'],
    # 'cache_control': '',
    'cache_expires': 0,
    'datasource': {'default_sort': [('name', 1)]},
    'schema': {
        'name': {
            'type': 'string',
            'required': True,
            'unique': True
        },
        'w': {
            'type': 'string',
            'required': True,
            # 'schema': {'type': 'number'}
        },
        'b': {
            'type': 'float',
            'required': True
        },
        'a': {
            'type': 'string',
            'required': True,
            # 'schema': {'type': 'number'}
        },
        'y': {
             'type': 'string',
             'required': True,
             # 'schema': {'type': 'number'}
        },
        'true_positive': {
            'type': 'number',
            'default': 0
        },
        'true_negative': {
            'type': 'number',
            'default': 0
        },
        'false_positive': {
            'type': 'number',
            'default': 0
        },
        'false_negative': {
            'type': 'number',
            'default': 0
        },
    }
}

DOMAIN = {
    'accounts': accounts,
    'images': images,
    'com': com,
    'steps': step_processed,
    'pconf': processing_config,
    'svm_model': svm_model,
    'histogram': histogram
}
