from flask import current_app as app
import numpy as np

def project(u, v):
    return (np.inner(v, u) * u / np.inner(u, u))


def gs_projection(vectors):
    """
    Gram-Schmidt Projection specialized just for image preprocessing
    only took one parameters. Vectors' shape = (3,3).
    """

    (v1, v2, v3) = vectors
    u2 = v2 - project(v1, v2)
    # app.logger.info(u2)
    # print(u2)
    w = v3 - project(v1, v3) - project(u2, v3)
    # w = v2 - project(v3, v1)
    # print(w)

    # u = np.zeros(vectors.shape)
    #
    # for i, v in enumerate(vectors):
    #     if i == 0:
    #         u[i] = v
    #     if i == 1:
    #         u[i] = vectors[-1] - project(u[i-1], vectors[-1])
    #     else:
    #         u[i] = u[i-1] - project(u[i-1], v)
    return w


def gs_projection2D(vectors):
    (v1, v2) = vectors
    return v2 - project(v1, v2)
