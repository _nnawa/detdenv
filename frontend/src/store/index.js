import Vue from 'vue'
import Vuex from 'vuex'

import { fetchImages, fetchStepped, fetchConfig, fetchModel, fetchHistogram } from '@/api'
import { sendCommand, upload, patchConfig, patchLabel } from '@/api'
import { removeData } from '@/api'

Vue.use(Vuex)

const state = {
  images: {
    _items: []
  },
  heroTitle: {
    HOME_TITLE: "Deteksi Virus Dengue",
    TRAINING_TITLE: "Training Data Citra Leukosit",
    INSPECT_TITLE: "Cek Data Citra",
    TESTING_TITLE: "Testing Data"
  },
  uploadState: {
    fileList: undefined,
    uploadError: undefined,
    currentStatus: 0,
    upLoadFieldName: 'raw_file',
    fileCount: {remaining:0, total: 0}
  },
  dialogState: false
  ,
  step_processed: {
    _items: []
  },
  processing_config: {
    _items: []
  },
  svm_model: {
    _items: [{_id:0, name: 'model-dummy'}]
  },
  histogram: {
    _items: [{
      _id: '0',
      hist: [0, 0, 0, .2, .3, .5, .6, .7, .8, .75, .55, .4, .2, 1, 0, 0],
      groups: ['hue', 'image'],
      ref_id: '0'
    }]
  },
  temp: {

  }
}

const actions = {
  loadImages(context, payload) {
    return fetchImages(payload)
      .then((response) => context.commit('setImages', {images: response.data}))
  },
  loadStepped(context, payload) {
    return fetchStepped(payload.where)
      .then((response) => context.commit('setStepProcessed', {step_processed: response.data}))
  },
  loadModel(context, payload) {
    return fetchModel(payload)
      .then((response) => context.commit('setModel', {svm_model: response.data}))
  },
  loadHistogram(context, payload) {
    return fetchHistogram()
      .then((response) => context.commit('setHistogram', {histogram: response.data}))
  },
  loadConfig(context, payload) {
    return fetchConfig(payload)
      .then((response) => context.commit('setConfig', {processing_config: response.data}))
  },
  deleteData(context, payload) {
    const headers = {
      'If-Match': context.state.temp._etag
    }
    const id = context.state.temp._id
    return removeData('images', headers, id)
      .then(response => context.commit('removeImage', {_id: id}))
  },
  processImages(context, command) {
    return sendCommand(context, command)
    .then((response) => context.dispatch('loadImages', {
      where: {is_processed: true},
      projection: { step_processed: 0}
    }))
    .then((response) => context.commit('setCurrentStatus', {currentStatus: 5}))
  },
  stepProcessImages(context, command) {
    return sendCommand(context, [command])
      .then((response) => context.dispatch('loadStepped', {
        where: {ref_id: command.ref_id}
      }))
  },
  computeHistogram(context, command) {
    return sendCommand(context, command)
      .then((response) => context.dispatch('loadHistogram', {
        projection: {hist:1, ref_id: 1, groups: 1}
      }))
  },
  trainData(context, command) {
    return sendCommand(context, [command])
  },
  testData(context, command) {
    return sendCommand(context, [command])
      .then((response) => context.dispatch('loadImages', {
        where: {is_trained: false, is_tested: true},
        projection: {step_processed: 0}
      }))
      .then((response) => context.commit('setCurrentStatus', {currentStatus: 9}))
  },
  uploadFiles(context, formData) {
    return upload(context, formData)
      // .then((response) => context.commit('setCurrentStatus', {currentStatus: 2}))
      // .then(()=> context.dispatch('loadImages', {
      //   where: {is_processed: false},
      //   projection: {detected: 0, step_processed:0}
      // }))
  },
  updateConfig(context, data) {
    if (data.hasOwnProperty('height') || data.hasOwnProperty('width')) {
      data = {
        image_size: {
          height: data.height ? data.height : context.getters.getConfig('height'),
          width: data.width ? data.width : context.getters.getConfig('width')
        }
      }
    }
    data = Object.assign({
      _id: context.getters.getConfig('_id'),
      _etag: context.getters.getConfig('_etag')
    }, data)
    return patchConfig(data)
      .then(response => context.dispatch('loadConfig'))
  },
  updateLabelAction(context, imidx) {
    const data = {
      labels: context.getters.getLabels(imidx),
      _id: context.state.images._items[imidx]._id,
      _etag: context.state.images._items[imidx]._etag,
    }
    return patchLabel(data)
  },
  updateLabel(context , payload) {
    return patchLabel(payload)
  },
  loadImageToTemp(context, payload) {
    return fetchImages(payload.filter, payload.id)
      .then((response) => context.commit('setTemp', {temp: response.data}))
  },
  analyze(context, payload) {
    return sendCommand(context, [payload])
      .then((response) => context.dispatch('loadModel', {
        projection: {w:0, b:0, a:0, y:0}
      }))
  }
}

const mutations = {
  setImages(state, payload) {
    state.images = payload.images
  },
  setDialogState(state, payload) {
    state.dialogState = payload.dialogState
  },
  setTemp(state, payload){
    state.temp = payload.temp
  },
  setStepProcessed(state, payload) {
    state.step_processed = payload.step_processed
  },
  setIsProcessed(state, payload) {
    for (var i = 0; i < state.images._items.length; i++) {
      state.images[i].is_processed = payload.is_processed
    }
  },
  setModel(state, payload) {
    state.svm_model = payload.svm_model
  },
  setHistogram(state, payload) {
    state.histogram = payload.histogram
  },
  setConfig(state, payload) {
    state.processing_config = payload.processing_config
  },
  updateLabel(state, payload) {
    const {id, imid, value} = payload
    state.images._items[imid].labels[id] = value
  },
  updateLabelsByName(state, payload) {
    const {filename, labels} = payload
    state.images._items.find(i => i.filename === filename).labels = labels
  },
  updateClientIsStepped(state, payload) {
    const {id, value} = payload
    state.images._items.find(item => item._id === id).is_stepped = value
  },

  // File Upload Management
  setFileCount(state, payload) {
    state.uploadState.fileCount = payload.fileCount
  },
  setCurrentStatus(state, payload) {
    state.uploadState.currentStatus = payload.currentStatus
  },
  resetForm(state) {
    // state.uploadState.uploadedFiles = []
    state.uploadState.currentStatus = 0
    state.uploadState.uploadError = undefined
  },
  setFileList(state, payload) {
    state.uploadState.fileList = payload.fileList
    state.uploadState.currentStatus = payload.status
  },
  setErrorResponse(state, payload) {
    state.uploadState.uploadError = payload.err
    state.uploadState.currentStatus = payload.status
  },
  resetImage(state) {
    state.images = []
  },
  removeImage(state, payload) {
    if (payload._id !== "") {
      state.images = {
        _items: state.images._items.filter(item => item._id !== payload._id)
      }
    } else {
      state.images = {
        _items: []
      }
      state.uploadState.currentStatus = 0
    }
  }
}

const getters = {
  getImageByName: state => filename => {
    return state.images._items.find(image => image.filename === filename)
  },
  getLabels: state => imidx => {
    return state.images._items[imidx].labels
  },
  getResultCount: state => cellClass => {
    var cellCount = 0, label = 0
    if (cellClass === "positive") {
      label = 1
    } else {
      label = -1
    }
    for (var i = 0; i < state.images._items.length; i++) {
      if (state.images._items[i].hasOwnProperty('result')) {
        cellCount += state.images._items[i].result.filter(i => i === label).length
      }
    }
    return cellCount
  },
  getHeroTitle: state => {
    return state.heroTitle
  },
  getCurrentStatus: state => {
    return state.uploadState.currentStatus
  },
  getFieldName: state => {
    return state.uploadState.upLoadFieldName
  },
  isStepped: state => imageId => {
    return state.images._items.find(image => image._id === imageId).is_stepped
  },
  getConfig: state => name => {
    if (name === 'height' || name === 'width') {
      return state.processing_config._items[0].image_size[name]
    } else {
      return state.processing_config._items[0][name]
    }
  },
  isHistogramExist: state => (ref_id, groups) => {
    return state.histogram._items.find(h => h.ref_id === ref_id) !== undefined
  },
  getHistogram: state => ref_id => {
    return state.histogram._items.filter(h => h.ref_id === ref_id)
  },
  getModelAnalysis: state => name => {
    return state.svm_model._items.find(m => m.name == name)
  }
}

const plugins = [
  (store) => {
    store.subscribe((mutation, state) => {
      let condition = (
        mutation.type === 'setCurrentStatus' &&
        mutation.payload.currentStatus !== 0 &&
        mutation.payload.currentStatus !== 1 &&
        state.images._items.length === 0
      )
      if (condition) {
        store.commit('setCurrentStatus', {currentStatus: 0})
      }
    })
  }
]

const store = new Vuex.Store({
  state,
  actions,
  mutations,
  getters,
  plugins
})


export default store
