import axios from 'axios'
import Vuex from 'vuex'

const API_URL = 'http://127.0.0.1:5000/api'
const config = {

}

export function removeData(resource, headers, id="") {
  const deleteConfig = Object.assign({
    headers: headers
  }, config)

  return axios.delete(`${API_URL}/${resource}/${id}`, deleteConfig)
}

export function fetchImages(filter, id="") {
  const fetchConfig = Object.assign({
    params: filter
  }, config)

  return axios.get(`${API_URL}/images/${id}`, fetchConfig)
}

export function fetchStepped(filter) {
  const fetchConfig = Object.assign({
    params: {
      where: filter,
    }
  }, config)

  return axios.get(`${API_URL}/steps`, fetchConfig)
}

export function fetchHistogram() {
  const fetchConfig = Object.assign({
    // params: filter
  }, config)

  return axios.get(`${API_URL}/histogram`, fetchConfig)
}

export function fetchModel(filter) {
  const fetchConfig = Object.assign({
    params: filter
  }, config)

  return axios.get(`${API_URL}/svm_model`, fetchConfig)
}

export function sendCommand(context, command) {
  const comConfig = Object.assign({
    headers: {'Content-Type': 'application/json'},
    transformRequest: [function bulkPost(data, headers) {
      const strdata = JSON.stringify(data)
      const charlist = strdata.split("")
      const uintArray = []
      for (var i = 0; i < charlist.length; i++) {
        uintArray.push(charlist[i].charCodeAt(0))
      }
      return new Uint8Array(uintArray)
    }]
  }, config)
  return axios.post(`${API_URL}/com/`, command, comConfig)
          .catch(err => context.commit('setErrorResponse', {err: err, status: 3}))
}

export function fetchConfig(payload) {
  return axios.get(`${API_URL}/pconf`, config)
}

export function patchConfig(data) {
  const pconfConfig = Object.assign({
    headers: {
      'If-Match': data._etag
    }
  }, config)
  const _id = data._id
  delete data._id
  delete data._etag
  return axios.patch(`${API_URL}/pconf/${_id}`, data, pconfConfig)
}

export function patchLabel(data) {
  const labelConfig = Object.assign({
    headers: {
      'If-Match': data._etag
    }
  }, config)
  const _id = data._id
  delete data._id
  delete data._etag
  return axios.patch(`${API_URL}/images/${_id}`, data, labelConfig)
}

export function upload(context, formData) {
  const uploadConfig = Object.assign({
    headers: {'Content-Type': 'multipart/form-data'}
  }, config)

  return axios.post(
    `${API_URL}/images/`,
    formData,
    uploadConfig
  ).catch(err => context.commit('setErrorResponse', {err: err, status: 3}))
}
